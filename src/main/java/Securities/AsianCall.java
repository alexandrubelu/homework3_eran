package Securities;

/**
 * Created by Alexandru on 12/7/2014.
 */

import RandomProcesses.StockPath;
import Utils.Pair;
import org.joda.time.DateTime;

import java.util.List;

public class AsianCall implements PayOut {

    private double strikePrice;

    public AsianCall(double strikePrice) {
        this.strikePrice = strikePrice;
    }

    public double getStrikePrice() 					 { return strikePrice; 			   }
    public void setStrikePrice( double strikePrice ) { this.strikePrice = strikePrice; }

    @Override
    public double getPayout( StockPath path ) {
        List<Pair<DateTime, Double>> price =  path.getPrices();

        double average = price.get(0).getValue();
        for ( int i=1; i != price.size(); i++ ) {
            average = i / ( i + 1.0 ) * average + price.get(i).getValue() / ( i + 1.0 );
        }
        return Math.max( average - strikePrice, 0 );
    }
}
