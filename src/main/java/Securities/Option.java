package Securities;

/**
 * Created by Alexandru on 12/7/2014.
 */

import org.joda.time.DateTime;
import org.joda.time.Period;

public class Option extends Security {

    // PRIVATE DATA MEMBERS
    private DateTime   _expirationTime;
    private Period     _period;
    private PayoutType _payout;
    private double     _interestRate;
    private double     _strikePrice;

    // CONSTRUCTORS
    public Option( String name,   double initialPrice, DateTime initialTime, double volatility,
                   Period period, PayoutType payout,   double interestRate,  double strikePrice    ) {
        // Basic Constructor.  Takes Initial Start Time and Period.  Calculates End Time from that information.
        super( name, initialPrice, initialTime, volatility );

        setPeriod		( period );
        setPayout		( payout );
        setInterestRate	( interestRate );
        setStrikePrice	( strikePrice );

        setExpirationTime( getPriceDateTime().plus( getPeriod() ) );
    }

    public Option( String name,             double initialPrice, DateTime initialTime, double volatility,
                   DateTime expirationTime, PayoutType payout,   double interestRate,  double strikePrice ) {
        // Basic Constructor.  Takes Initial Start Time and Period.  Calculates End Time from that information.
        super( name, initialPrice, initialTime, volatility );

        setExpirationTime( expirationTime );
        setInterestRate  ( interestRate );
        setVolatility    ( volatility );
        setStrikePrice   ( strikePrice );

        setPeriod( new Period(getPriceDateTime(), getExpirationTime()) );
    }

    public DateTime   getExpirationTime() { return _expirationTime; }
    public Period     getPeriod()		  { return _period; }
    public PayoutType getPayout()		  { return _payout; }
    public double 	  getInterestRate()   { return _interestRate; }
    public double     getStrikePrice()    { return _strikePrice; }

    public void setExpirationTime( DateTime expireTime ) { this._expirationTime = expireTime; }
    public void setPeriod		 ( Period period )		 { this._period = period; }
    public void setPayout		 ( PayoutType payout )   { this._payout = payout; }
    public void setInterestRate  ( double interestRate ) { this._interestRate = interestRate; }
    public void setStrikePrice	 ( double strikePrice )  { this._strikePrice = strikePrice; }
}
