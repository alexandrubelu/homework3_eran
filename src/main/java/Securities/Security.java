package Securities;

/**
 * Created by Alexandru on 12/7/2014.
 */
import org.joda.time.DateTime;

public class Security {
// Generic Security Class.  The basic building block of all financial Securities out there.
// Will serve the basis for our Option Class, but can also be used in the future to
// create Stocks, Bonds, Derivatives, etc.

    //PRIVATE DATA MEMBERS
    private double   _price;
    private double   _volatility;
    private String   _name; // security identifier (ticker, cusip, isin, etc.)
    private DateTime _priceDateTime; // Date and Time for which the Security price and volatility were calculated.
    // On different dates and time these can be different.

    // CONSTRUCTORS
    public Security(  String name, double price, DateTime dateTime, double volatility ) {
        setName	        ( name 	  );
        setPrice	    ( price 	  );
        setPriceDateTime( dateTime   );
        setVolatility	( volatility );
    }

    // GETTERS
    public double   getPrice()  	   { return _price; }
    public double   getVolatility()    { return _volatility; }
    public String   getName() 		   { return _name; }
    public DateTime getPriceDateTime() { return _priceDateTime; }

    // SETTERS
    public void setPrice     ( double price) 	        { this._price = price; }
    public void setVolatility( double volatility )      { this._volatility = volatility; }
    public void setName		 ( String name )            { this._name = name; }
    public void setPriceDateTime  ( DateTime dateTime ) { this._priceDateTime = dateTime;}
}
