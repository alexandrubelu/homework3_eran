package Securities;

/**
 * Created by Alexandru on 12/7/2014.
 */

import java.util.List;

import org.joda.time.DateTime;

import RandomProcesses.StockPath;
import Utils.Pair;

public class EuropeanCall implements PayOut {

    private double strikePrice;

    public EuropeanCall (double strikePrice) {
        this.strikePrice = strikePrice;
    }

    public double getStrikePrice() 					   { return strikePrice; 		     }
    public void   setStrikePrice( double strikePrice ) { this.strikePrice = strikePrice; }


    @Override
    public double getPayout(StockPath path) {
        List<Pair<DateTime, Double>> price =  path.getPrices();
        return Math.max( price.get( price.size() - 1 ).getValue() - strikePrice, 0 );
    }
}