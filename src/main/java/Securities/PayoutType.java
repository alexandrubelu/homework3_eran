package Securities;

/**
 * Created by Alexandru on 12/7/2014.
 */

public class PayoutType {

    public static final PayoutType AMERICAN = new PayoutType( "American" );
    public static final PayoutType ASIAN    = new PayoutType( "Asian"    );
    public static final PayoutType EUROPEAN = new PayoutType( "European" );

    private String _payoutType;

    private PayoutType( String payoutType ) { _payoutType = payoutType; }

    public String toString() { return ( String.format( "PayoutType(%s)", _payoutType ) ); }

}
