package Securities;

/**
 * Created by Alexandru on 12/7/2014.
 */
import RandomProcesses.StockPath;

public interface PayOut{

    public double getPayout( StockPath path ) ;

}