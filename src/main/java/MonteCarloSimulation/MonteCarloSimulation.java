package MonteCarloSimulation;

/**
 * Created by Alexandru on 12/7/2014.
 */
import org.joda.time.DateTime;
import org.joda.time.Period;

import RandomProcesses.GeometricBrownianMotion;
import RandomVectors.AntiTheticVectorGenerator;
import RandomVectors.GaussianRandomVectorGenerator;
import RandomVectors.RandomVectorGenerator;
import Securities.EuropeanCall;
import Securities.Option;
import Securities.PayOut;
import Securities.PayoutType;
import Utils.SimpleStatistics;

public class MonteCarloSimulation {

    public void euroCall(){
        System.out.println("Running European Call Simulation: ");

        String optionName 	  = "IBM";
        double confidence 	  = 0.96;
        double error	 	  = 0.01;
        int optionDays 		  = 252;
        double initialPrice   = 152.35;
        double interestRate   = 0.0001;
        double volatility     = 0.01;
        double strikePrice    = 165;
        PayoutType payOutType = PayoutType.EUROPEAN;

        DateTime initialTime= new DateTime(2014,10,19,21,56);
        Period period = Period.days(optionDays);

        Option option = new Option(optionName, initialPrice, initialTime, volatility, period, payOutType, interestRate, strikePrice);

        RandomVectorGenerator gaussianGenerator = new GaussianRandomVectorGenerator();
        RandomVectorGenerator generator 	    = new AntiTheticVectorGenerator( gaussianGenerator );
        GeometricBrownianMotion pathGenerator   = new GeometricBrownianMotion( option, generator );

        SimpleStatistics stat = new SimpleStatistics();

        PayOut payOut = new EuropeanCall( strikePrice );

        double value = SimulationManager.simulate(pathGenerator, payOut, stat, confidence, error);
        double price = value * Math.exp(- option.getInterestRate() * option.getPeriod().getDays() );
        System.out.println("The value of option in expiration date is :" + value);
        System.out.println("The option should be priced as :" + price);

        System.out.println("================================");
    }

    public void asiaCall(){
        System.out.println("Running Asian Call Simulation: ");

        String optionName 	  = "IBM";
        double confidence 	  = 0.96;
        double error	 	  = 0.01;
        int optionDays 		  = 252;
        double initialPrice   = 152.35;
        double interestRate   = 0.0001;
        double volatility     = 0.01;
        double strikePrice    = 164;
        PayoutType payOutType = PayoutType.ASIAN;

        DateTime initialTime= new DateTime(2014,10,19,21,56);
        Period period = Period.days(optionDays);

        Option option = new Option(optionName, initialPrice, initialTime, volatility, period, payOutType, interestRate, strikePrice);

        RandomVectorGenerator gaussianGenerator = new GaussianRandomVectorGenerator();
        RandomVectorGenerator generator 	    = new AntiTheticVectorGenerator( gaussianGenerator );
        GeometricBrownianMotion pathGenerator   = new GeometricBrownianMotion( option, generator );

        SimpleStatistics stat = new SimpleStatistics();

        PayOut payOut = new EuropeanCall( strikePrice );

        double value = SimulationManager.simulate(pathGenerator, payOut, stat, confidence, error);
        double price = value * Math.exp(- option.getInterestRate() * option.getPeriod().getDays() );
        System.out.println("The value of option in expiration date is :" + value);
        System.out.println("The option should be priced as :" + price);



        System.out.println("=============================");
    }

    public static void main(String[] args) {

        MonteCarloSimulation simulation = new MonteCarloSimulation();
        simulation.euroCall();;
        simulation.asiaCall();
    }
}
