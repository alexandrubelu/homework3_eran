package MonteCarloSimulation;

/**
 * Created by Alexandru on 12/7/2014.
 */

import org.apache.commons.math3.distribution.NormalDistribution;
import org.joda.time.DateTime;

import RandomProcesses.StockPath;
import Securities.PayOut;
import Utils.SimpleStatistics;

public class SimulationManager {

    public static double simulate( StockPath path, PayOut payOut, SimpleStatistics stats, double confidence, double errorTolerance ) {
        double bound = - new NormalDistribution().inverseCumulativeProbability( (1 - confidence) / 2 ) ;

        int maxIterations = 30000000;//1000000;
        DateTime start = new DateTime();
        System.out.println("Start Time: " + start.getHourOfDay() + ":" + start.getMinuteOfHour() + ":" + start.getSecondOfMinute());
        System.out.println("Progress: ");
        for (int i=1; i < maxIterations; i++) {
            stats.addSample( payOut.getPayout( path ) );
            //System.out.println(i+"-th simulation with error: "+bound * stat.getStdVar()/ Math.sqrt(i)+"   with mean: "+stat.getMean()+"    with standard deviation "+stat.getStdVar());
            int percentValue = (int) ( maxIterations / 100 ); // Progress updated every 10%
            //double estEnding = bound * stats.getStdDev() / accuRate;
            if ( i % percentValue == 0 ) {
                System.out.println( ( i / percentValue ) + "%" );
            }

            if ( bound * stats.getStdDev() / Math.sqrt(i) < errorTolerance && i > percentValue ) {
                System.out.println( i + " times simulations to converge!");
                break;
            }
        }

        DateTime end = new DateTime();
        System.out.println("End Time: " + end.getHourOfDay() + ":" + end.getMinuteOfHour() + ":" + end.getSecondOfMinute());

        return stats.getMean();
    }
}