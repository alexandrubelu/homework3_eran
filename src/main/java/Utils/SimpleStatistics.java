package Utils;

/**
 * Created by Alexandru on 12/7/2014.
 */
import java.util.ArrayList;

public class SimpleStatistics {

    private ArrayList<Double> array;

    private double firstMoment        = 0.0; // first moment
    private double standardDeviation  = 0.0; // central second moment
    private double secondMoment       = 0.0; // second moment

    public SimpleStatistics() {
        this.array = new ArrayList<Double>();
    }

    public double getMean()   { return firstMoment;       }
    public double getStdDev() { return standardDeviation; }

    public void addSample( double x ) {
        int n = array.size(); // Current Number of Samples
        array.add( new Double(x) );

        firstMoment       = ( x + n * firstMoment ) / ( n + 1 );
        secondMoment      = ( x*x + n * secondMoment ) / ( n + 1 );
        standardDeviation = Math.sqrt( secondMoment - firstMoment*firstMoment );
    }

}