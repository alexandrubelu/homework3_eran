package RandomProcesses;

/**
 * Created by Alexandru on 12/7/2014.
 */

import RandomVectors.RandomVectorGenerator;
import Securities.Option;
import Utils.Pair;
import org.joda.time.DateTime;

import java.util.LinkedList;
import java.util.List;

public class GeometricBrownianMotion implements StockPath {

    private double		  _mu;
    private double		  _sigma;
    private int           _numPointsToSample;
    private int 	 	  _deltaT;
    private double		  _startingPoint;
    private DateTime 	  _startingTime;
    RandomVectorGenerator _randomVectorGenerator;

    public GeometricBrownianMotion( double mu, double sigma, int numPoints, double startPoint, DateTime startTime, DateTime endTime, RandomVectorGenerator rvg ) {
        this._mu 					= mu;
        this._sigma 				= sigma;
        this._numPointsToSample     = numPoints;
        this._deltaT 				= (int) ( (endTime.getMillis() - startTime.getMillis()) / this._numPointsToSample );
        this._startingPoint 		= startPoint;
        this._startingTime      	= startTime;
        this._randomVectorGenerator = rvg;
    }

    public GeometricBrownianMotion( Option option, RandomVectorGenerator rvg ) {
        this._mu 					= option.getInterestRate();
        this._sigma 				= option.getVolatility();
        this._numPointsToSample     = option.getPeriod().getDays();
        this._deltaT 				= (int) ( (option.getExpirationTime().getMillis() - option.getPriceDateTime().getMillis()) / this._numPointsToSample );
        this._startingPoint 		= option.getPrice();
        this._startingTime      	= option.getPriceDateTime();
        this._randomVectorGenerator = rvg;
    }

    @Override
    public List<Pair<DateTime, Double>> getPrices() {

        float[] randVect = _randomVectorGenerator.getVector( this._numPointsToSample );
        DateTime currentTime  = new DateTime( _startingTime.getMillis() );

        List<Pair<DateTime, Double>> path = new LinkedList<Pair<DateTime,Double>>();
        path.add( new Pair<DateTime, Double>( currentTime, _startingPoint ) );

        for ( int i=1; i != randVect.length; i++ ){
            currentTime = currentTime.plusMillis(_deltaT);
            double previousPoint = path.get( path.size() - 1 ).getValue();
            path.add( new Pair<DateTime, Double>( currentTime, previousPoint * Math.exp((_mu - _sigma*_sigma / 2) + _sigma * randVect[i-1]) ) );
        }

        return path;
    }
}
