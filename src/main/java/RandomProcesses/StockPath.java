package RandomProcesses;

/**
 * Created by Alexandru on 12/7/2014.
 */
import java.util.List;

import org.joda.time.DateTime;

import Utils.Pair;

public interface StockPath {

    public List<Pair<DateTime, Double>> getPrices();

}