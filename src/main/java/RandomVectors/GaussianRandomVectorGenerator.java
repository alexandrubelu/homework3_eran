package RandomVectors;

/**
 * Created by Alexandru on 12/7/2014.
 */

import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;

import java.util.Random;

import static org.bridj.Pointer.pointerToFloats;

public class GaussianRandomVectorGenerator implements RandomVectorGenerator {

    // PRIVATE DATA MEMBERS
    private Random _rand = new Random();
    private double[] _mean;
    private double[] _variance;
    private int _dimensions = 10000000;
    private int _index      = 0;
    private float[] _values;

    // CONSTRUCTORS
    public GaussianRandomVectorGenerator() {
        // Basic Constructor - Uses System Time as random seed.
        // Input the dimensions of the Random Generator.
        // Output Standard Gaussian Random Vector (mean 0, standard deviation 1) in dimensions = dimensions.

        generateRandomVariables();
        //this( (int) System.currentTimeMillis() );
    }

    // PUBLIC METHODS
    @Override //RandomVectorGenerator Override
    public float[] getVector( int numPoints ) {
        //uniformToNormal( _generator.nextVector() );
        //return _generator.nextVector();
        if( numPoints < (_dimensions - _index) ) {

            return getPoints( numPoints );
        } else {

            if( numPoints > _dimensions ) {

                // This is a lazy check to see if we are being asked to generate far too many points.
                // Something more intelligent should be done here to ensure we're not trying to generate more points
                // then our system can handle.
                System.out.println( "Cannot Generate This Many Points.");
                return new float[0];
            }

            generateRandomVariables();

            _index  = 0;
            return getPoints( numPoints );
        }
    }

    private float[] getPoints( int numPoints ) {

        float[] output = new float[numPoints];
        for( int i = 0; i != numPoints; i++ ) {

            output[i] = _values[_index + i];
        }

        _index += numPoints;
        return output;
    }

    private void generateRandomVariables() {

        float[] A = new float[_dimensions];
        float[] B = new float[_dimensions];
        for( int i=0; i != _dimensions; i++ ) {

            A[i] = _rand.nextFloat();
            B[i] = _rand.nextFloat();
        }

        _values = uniformToNormal(A, B);
    }

    // PRIVATE METHODS
    private float[] uniformToNormal( float[] uniformVectorA, float[] uniformVectorB ) {

        // ASSUMES EQUAL SIZE VECTORS
        if (uniformVectorA.length != uniformVectorB.length) {

            System.out.println("ERROR: Need equal size uniform Vectors");
            return new float[0];
        }

        int numPoints = uniformVectorA.length;
        // Creating the platform which is out computer.
        CLPlatform clPlatform = JavaCL.listPlatforms()[0];
        // Getting the GPU device
        CLDevice device = clPlatform.getBestDevice();
        // Let's make a context
        CLContext context = JavaCL.createContext(null, device);
        // Lets make a default FIFO queue.
        CLQueue queue = context.createDefaultQueue();

        String src = "__kernel void uniform_to_normal(__global const float* a, __global const float* b, __global float* x, __global float* y, int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i >= n )\n" +
                "        return;\n" +
                "\n" +
                "    float r = sqrt(-2*log(a[i]));\n" +
                "    float theta = 2*M_PI*b[i];\n" +
                "    x[i] = r*sin(theta);\n" +
                "    y[i] = r*cos(theta);\n" +
                "}";

        CLProgram program = context.createProgram(src);
       // System.out.println( "TEST" );
        program.build();


        CLKernel kernel = program.createKernel("uniform_to_normal");

        final Pointer<Float>
                aPtr = pointerToFloats(uniformVectorA),
                bPtr = pointerToFloats(uniformVectorB);//



        // Create OpenCL input buffers (using the native memory pointers aPtr and bPtr) :
        CLBuffer<Float>
                a = context.createFloatBuffer(CLMem.Usage.Input, aPtr),
                b = context.createFloatBuffer(CLMem.Usage.Input, bPtr);

        // Create an OpenCL output buffer :
        CLBuffer<Float>
                x = context.createFloatBuffer(CLMem.Usage.Output, numPoints),
                y = context.createFloatBuffer(CLMem.Usage.Output, numPoints);

        kernel.setArgs( a, b, x, y, numPoints );
        CLEvent event = kernel.enqueueNDRange(queue, new int[]{numPoints});


        final Pointer<Float> xPtr = x.read(queue, event);
        final Pointer<Float> yPtr = y.read(queue, event);

        float[] output = new float[ numPoints + numPoints ];
        int index = 0;
        for( int i=0; i != numPoints; i++ ) {

            output[index] = xPtr.get(i);
            ++index;
            output[index] = yPtr.get(i);
        }

        return output;
    }
}
