package RandomVectors;

/**
 * Created by Alexandru on 12/7/2014.
 */
public interface RandomVectorGenerator {

    public float[] getVector( int numPoints);

}
