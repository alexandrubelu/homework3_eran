package RandomVectors;

/**
 * Created by Alexandru on 12/7/2014.
 */
import java.util.Arrays;

public class AntiTheticVectorGenerator implements RandomVectorGenerator{

    // DATA MEMBERS
    private RandomVectorGenerator rvg;
    float[] lastVector;

    // CONSTRUCTORS
    public AntiTheticVectorGenerator(RandomVectorGenerator rvg){
        this.rvg = rvg;
    }

    // PUBLIC METHODS
    @Override
    public float[] getVector( int numPoints ) {
        if ( lastVector == null ){
            lastVector = rvg.getVector( numPoints );
            return lastVector;
        } else {
            float[] tmp =Arrays.copyOf(lastVector, lastVector.length);
            lastVector = null;
            for (int i = 0; i < tmp.length; ++i){ tmp[i] = -tmp[i];}
            return tmp;
        }
    }

}